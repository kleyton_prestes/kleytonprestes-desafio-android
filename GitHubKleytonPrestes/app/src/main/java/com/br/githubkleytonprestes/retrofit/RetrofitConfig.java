package com.br.githubkleytonprestes.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.br.githubkleytonprestes.Utils.Constants.BASE_URL;

/**
 * Created by kleyton on 24/06/17.
 */

public class RetrofitConfig {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static ServicesRetrofit getServicesRetrofit() {
        return getClient().create(ServicesRetrofit.class);
    }

}
