package com.br.githubkleytonprestes.retrofit;

import com.br.githubkleytonprestes.model.Item;
import com.br.githubkleytonprestes.model.Pull;
import com.br.githubkleytonprestes.model.RepositoriesGit;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by kleyton on 24/06/17.
 */

public interface ServicesRetrofit {

    @GET("search/repositories?q=language:Java&sort=stars&")
    Call<RepositoriesGit> getRepositories(@Query("page") String page);

    @GET("repos/{creator}/{repositorie}/pulls")
    Call<List<Pull>> getPulls(@Path("creator") String creator, @Path("repositorie") String repositorie);


}
