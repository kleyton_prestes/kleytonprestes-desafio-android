package com.br.githubkleytonprestes.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.githubkleytonprestes.R;
import com.br.githubkleytonprestes.model.Pull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by kleyton on 26/06/17.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.PullViewHolder> {

    private Context context;

    private LayoutInflater inflater;

    private List<Pull> pullList;

    public PullAdapter(Context context, List<Pull> pullList) {
        this.context = context;
        this.pullList = pullList;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public PullViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.custom_row_pull, parent, false);

        PullViewHolder holder = new PullViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final PullViewHolder holder, int position) {

        final Pull pull = pullList.get(position);

        holder.pullTitle.setText(pull.getTitle());
        holder.pullDescription.setText(pull.getBody());
        holder.pullUserName.setText(pull.getUser().getLogin());
        holder.pullNameLastname.setText(pull.getUser().getLogin());


        Glide.with(context).load(pull.getUser().getAvatarUrl()).asBitmap().centerCrop().skipMemoryCache(false).into(new BitmapImageViewTarget(holder.userPhoto) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.userPhoto.setImageDrawable(circularBitmapDrawable);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.getHtmlUrl()));
                context.startActivity(browserIntent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return pullList != null ? pullList.size() : 0;
    }

    class PullViewHolder extends RecyclerView.ViewHolder{

        TextView pullTitle;
        TextView pullDescription;
        TextView pullUserName;
        TextView pullNameLastname;

        ImageView userPhoto;

        public PullViewHolder(View itemView) {
            super(itemView);

            pullTitle = (TextView) itemView.findViewById(R.id.pull_title);
            pullDescription = (TextView) itemView.findViewById(R.id.pull_description);
            pullUserName = (TextView) itemView.findViewById(R.id.pull_user_name);
            pullNameLastname = (TextView) itemView.findViewById(R.id.pull_name_lastname);

            userPhoto = (ImageView) itemView.findViewById(R.id.user_photo);


        }
    }

}
