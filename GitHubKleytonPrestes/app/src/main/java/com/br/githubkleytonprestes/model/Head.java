package com.br.githubkleytonprestes.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kleyton on 26/06/17.
 */

public class Head {

    @SerializedName("label")
    private String label;

    @SerializedName("ref")
    private String ref;

    @SerializedName("sha")
    private String sha;

    @SerializedName("user")
    private User user;

    @SerializedName("repo")
    private Item item;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSha() {
        return sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
