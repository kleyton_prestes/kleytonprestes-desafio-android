package com.br.githubkleytonprestes.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.githubkleytonprestes.R;
import com.br.githubkleytonprestes.fragments.PullFragment;
import com.br.githubkleytonprestes.model.Item;
import com.br.githubkleytonprestes.model.RepositoriesGit;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kleyton on 25/06/17.
 */

public class RepositorieAdapter extends RecyclerView.Adapter<RepositorieAdapter.MyViewHolder> {

    private Context context;

    private LayoutInflater inflater;

    private List<Item> items;

    private MyRepositorieClick myRepositorieClick;

    public interface MyRepositorieClick {
        void onClick(Item item);
    }


    public RepositorieAdapter(Context context) {

        this.context = context;
        inflater = LayoutInflater.from(context);

    }

    public void setOnclick(MyRepositorieClick onclick){
        this.myRepositorieClick = onclick;

    }

    public void setData(List<Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row_repositorie, parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Item item = items.get(position);

        holder.nameRepositorie.setText(item.getName());


        holder.descriptionRepositorie.setText(item.getDescription());
        holder.countFork.setText(String.valueOf(item.getForksCount()));
        holder.countStar.setText(String.valueOf(item.getStargazersCount()));
        holder.userName.setText(item.getOwner().getLogin());
        holder.nameLastName.setText(item.getOwner().getLogin());

        Glide.with(context).
                load(item.getOwner().getAvatarUrl()).
                asBitmap().
                centerCrop().
                skipMemoryCache(false).
                into(new BitmapImageViewTarget(holder.userPhoto) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.userPhoto.setImageDrawable(circularBitmapDrawable);
                    }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myRepositorieClick != null)
                    myRepositorieClick.onClick(item);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nameRepositorie;
        TextView descriptionRepositorie;
        TextView countFork;
        TextView countStar;
        TextView userName;
        TextView nameLastName;
        ImageView userPhoto;

        public MyViewHolder(View itemView) {
            super(itemView);

            nameRepositorie = (TextView) itemView.findViewById(R.id.name_repositorie);
            descriptionRepositorie = (TextView) itemView.findViewById(R.id.description_repositorie);
            countFork = (TextView) itemView.findViewById(R.id.count_fork);
            countStar = (TextView) itemView.findViewById(R.id.count_star);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            nameLastName = (TextView) itemView.findViewById(R.id.name_lastname);
            userPhoto = (ImageView) itemView.findViewById(R.id.user_photo);

        }
    }

}
