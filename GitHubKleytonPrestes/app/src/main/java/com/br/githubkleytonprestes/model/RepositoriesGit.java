package com.br.githubkleytonprestes.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kleyton on 25/06/17.
 */

public class RepositoriesGit {
    @SerializedName("total_count")
    private int totalCount;
    @SerializedName("incomplete_results")
    private String incompleteResults;
    @SerializedName("items")
    private List<Item> items;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(String incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
