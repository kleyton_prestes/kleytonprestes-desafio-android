package com.br.githubkleytonprestes.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.br.githubkleytonprestes.R;
import com.br.githubkleytonprestes.Utils.PaginationScrollListener;
import com.br.githubkleytonprestes.Utils.Utils;
import com.br.githubkleytonprestes.activities.MainActivity;
import com.br.githubkleytonprestes.adapters.RepositorieAdapter;
import com.br.githubkleytonprestes.model.Item;
import com.br.githubkleytonprestes.model.RepositoriesGit;
import com.br.githubkleytonprestes.retrofit.RetrofitConfig;
import com.br.githubkleytonprestes.retrofit.ServicesRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kleyton on 25/06/17.
 */

public class Mainfragment extends Fragment implements RepositorieAdapter.MyRepositorieClick{

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;

    private RepositorieAdapter repositorieAdapter;
    private ProgressBar progressBar;

    private List<Item> items;

    private Call<RepositoriesGit> call;
    private ServicesRetrofit servicesRetrofit;
    private int pages = 1;

    private PaginationScrollListener scrollListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recycler_fragment, container, false);

        Utils.setupActionBar(getActivity(), false);
        ((MainActivity)getActivity()).setActionBarTitle("GitHub");

        items = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.repositories_list);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        manager = new LinearLayoutManager(getContext());

        repositorieAdapter = new RepositorieAdapter(getContext());
        repositorieAdapter.setOnclick(this);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(repositorieAdapter);
        getRepositories();


        scrollListener = new PaginationScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                progressBar.setVisibility(View.VISIBLE);
                getRepositories();
            }
        };

        recyclerView.setOnScrollListener(scrollListener);


        return view;
    }

    @Override
    public void onClick(Item item) {
        PullFragment mainfragment = new PullFragment().newInstance(item);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, mainfragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void getRepositories(){
        servicesRetrofit = RetrofitConfig.getServicesRetrofit();

        call = servicesRetrofit.getRepositories(String.valueOf(pages));

        call.enqueue(new Callback<RepositoriesGit>() {
            @Override
            public void onResponse(Call<RepositoriesGit> call, Response<RepositoriesGit> response) {
                if (response.isSuccessful() && response.body() != null) {

                    RepositoriesGit repositoriesGit =  response.body();
                    items.addAll(repositoriesGit.getItems());
                    repositorieAdapter.setData(items);
                    pages++;

                }

                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<RepositoriesGit> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
