package com.br.githubkleytonprestes.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kleyton on 26/06/17.
 */

public class Pull {

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("user")
    private User user;

    @SerializedName("head")
    private Head head;

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Head getHead() {
        return head;
    }

    public void setHead(Head head) {
        this.head = head;
    }
}
