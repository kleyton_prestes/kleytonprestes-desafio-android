package com.br.githubkleytonprestes.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.br.githubkleytonprestes.R;
import com.br.githubkleytonprestes.Utils.Utils;
import com.br.githubkleytonprestes.activities.MainActivity;
import com.br.githubkleytonprestes.adapters.PullAdapter;
import com.br.githubkleytonprestes.adapters.RepositorieAdapter;
import com.br.githubkleytonprestes.model.Item;
import com.br.githubkleytonprestes.model.Pull;
import com.br.githubkleytonprestes.retrofit.RetrofitConfig;
import com.br.githubkleytonprestes.retrofit.ServicesRetrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kleyton on 26/06/17.
 */

public class PullFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager manager;

    private PullAdapter pullAdapter;

    private Call<List<Pull>> call;

    private ServicesRetrofit servicesRetrofit;

    public PullFragment newInstance(Item item) {

        PullFragment pullFragment = new PullFragment();

        Bundle args = new Bundle();
        args.putString("repositorie_name", item.getName());
        args.putString("creator", item.getOwner().getLogin());

        pullFragment.setArguments(args);

        return pullFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recycler_fragment, container, false);
        String creator = "";
        String repositorieName = "";
        setHasOptionsMenu(true);
        Utils.setupActionBar(getActivity(), true);

        if (getArguments() != null) {
            repositorieName = getArguments().getString("repositorie_name");
            creator = getArguments().getString("creator");
        }
        ((MainActivity)getActivity()).setActionBarTitle(repositorieName);

        recyclerView = (RecyclerView) view.findViewById(R.id.repositories_list);

        manager = new LinearLayoutManager(getContext());

        servicesRetrofit = RetrofitConfig.getServicesRetrofit();

        call = servicesRetrofit.getPulls(creator, repositorieName);

        call.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {

                if (response.isSuccessful() && response.body() != null) {
                    List<Pull> pullList = (List<Pull>) response.body();

                    pullAdapter = new PullAdapter(getContext(), pullList);

                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(pullAdapter);
                }

            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                t.printStackTrace();

            }
        });

        return view;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getFragmentManager().popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
