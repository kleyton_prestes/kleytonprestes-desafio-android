package com.br.githubkleytonprestes.Utils;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.br.githubkleytonprestes.R;

/**
 * Created by kleyton on 26/06/17.
 */

public class Utils {
    public static void setupActionBar(Activity activity, Boolean enabled) {
        ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
//        actionBar.setHomeAsUpIndicator(R.drawable.arrow_back);
        actionBar.setDisplayShowCustomEnabled(enabled);
        actionBar.setHomeButtonEnabled(enabled);
        actionBar.setDisplayHomeAsUpEnabled(enabled);


    }
}
